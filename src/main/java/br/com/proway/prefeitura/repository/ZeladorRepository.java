package br.com.proway.prefeitura.repository;

import br.com.proway.prefeitura.model.Zelador;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

public interface ZeladorRepository extends JpaRepository<Zelador, Long> {

    ArrayList<Zelador> findAll();

    Zelador save(Zelador zelador);
    void deleteById(long id);

}
