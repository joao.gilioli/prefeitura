package br.com.proway.prefeitura.controllers;

import br.com.proway.prefeitura.model.Zelador;
import br.com.proway.prefeitura.repository.ZeladorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/zelador")
public class ZeladorController {

    @Autowired
    private ZeladorRepository zeladorRepository;

    @GetMapping("/zeladores")
    public ArrayList<Zelador> getAllZelador() {
        return zeladorRepository.findAll();
    }

    @PostMapping("/add")
    public ResponseEntity<Zelador> addZelador(@RequestBody Zelador zelador) {
        Zelador zAux;
        try {
            zAux = zeladorRepository.save(zelador);
            return new ResponseEntity<>(null, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    public void deleteZelador(@PathVariable("id") Long id){
        zeladorRepository.deleteById(id);
    }

    @PutMapping("/{id}")
    public Zelador updateZelador(@PathVariable("id") Long id, @RequestBody Zelador zelador) {
        zelador.setId(id);
        return zeladorRepository.save(zelador);
    }

}
